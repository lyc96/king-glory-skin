import pandas as pd
from collections import Counter

df = pd.read_excel("王者荣耀英雄皮肤.xlsx")


price = df["点券"].tolist()
tp = df["类型"].tolist()
c_888 =[]
c_1788 =[]
c_0 =[]
c_1688 =[]
for i in range(0,len(price)):
    if str(price[i])=="888":
        if tp[i] not in c_888:
           c_888.append(tp[i])
    if str(price[i])=="1788":
        if tp[i] not in c_1788:
           c_1788.append(tp[i])
    if str(price[i])=="0":
        if tp[i] not in c_0:
           c_0.append(tp[i])
    if str(price[i])=="1688":
        if tp[i] not in c_1688:
           c_1688.append(tp[i])
"""
888 ['限定', '史诗', 'KPL限定', '五五开黑节', '情人节限定', '勇者', '猪年限定', '其他', '二周年限定']
1788 ['限定', '情人节限定', '牛年限定', 'KPL限定', '鼠年限定', '猪年限定', '狗年限定']
0 ['其他', '赛季限定', '五周年限定', '四周年限定', '勇者', '三周年限定', '战队赛专属']
1688 ['限定', 'FMVP', '传说', '其他']
"""
print("888",c_888)
print("1788",c_1788)
print("0",c_0)
print("1688",c_1688)

def an4():
    tp = df["类型"].tolist()
    clear_tp = [str(i) for i in tp]
    result = Counter(clear_tp)
    # 排序
    d = sorted(result.items(), key=lambda x: x[1], reverse=True)
    t = [i[0] for i in d]
    v = [i[1] for i in d]
    """
    ['史诗', '勇者', '限定', '战令限定', '其他', '赛季限定', 'KPL限定', 
    'FMVP', '荣耀典藏', '情人节限定', '鼠年限定', '猪年限定', '牛年限定', 
    '五五开黑节', '传说', '狗年限定', '五周年限定', '四周年限定', '三周年限定', '战队赛专属', '二周年限定']
    [27, 27, 26, 21, 20, 18, 9, 6, 6, 5, 5, 5, 4, 2, 2, 2, 1, 1, 1, 1, 1]
    """
    print(t)
    print(v)

def an3():
    price = df["点券"].tolist()
    clear_price = [str(i) for i in price]
    result = Counter(clear_price)
    # 排序
    d = sorted(result.items(), key=lambda x: x[1], reverse=True)
    t = [i[0] for i in d]
    v = [i[1] for i in d]
    """
    ['888', '1788', '0', '388', '1688', '60', '夺宝', '488', '抽奖', '贵族限定', '660']
    [52, 30, 28, 21, 19, 17, 13, 6, 2, 1, 1]
    """
    print(t)
    print(v)

def an2():
    time = df["上线时间"].tolist()
    t_2017 = []
    t_2018 = []
    t_2019 = []
    t_2020 = []
    t_2021 = []
    for j in [str(i)[0:6] for i in time]:
        if "2017" in str(j):
            t_2017.append(j[4:6])
        if "2018" in str(j):
            t_2018.append(j[4:6])
        if "2019" in str(j):
            t_2019.append(j[4:6])
        if "2020" in str(j):
            t_2020.append(j[4:6])
        if "2021" in str(j):
            t_2021.append(j[4:6])
    # 排序
    d_2017 = sorted(Counter(t_2017).items(), key=lambda x: x[1], reverse=True)
    d_2018 = sorted(Counter(t_2018).items(), key=lambda x: x[1], reverse=True)
    d_2019 = sorted(Counter(t_2019).items(), key=lambda x: x[1], reverse=True)
    d_2020 = sorted(Counter(t_2020).items(), key=lambda x: x[1], reverse=True)
    d_2021 = sorted(Counter(t_2021).items(), key=lambda x: x[1], reverse=True)
    print("2017",d_2017[0])
    print("2018",d_2018[0])
    print("2019",d_2019[0])
    print("2020",d_2020[0])
    print("2021",d_2021[0])
    """
    2017 ('10', 3)
    2018 ('10', 6)
    2019 ('02', 10)
    2020 ('01', 14)
    2021 ('02', 10)
    """

def an1():
    time = df["上线时间"].tolist()
    clear_time = [str(i)[0:4] for i in time]
    result = Counter(clear_time)
    # 排序
    d = sorted(result.items(), key=lambda x: x[1], reverse=True)
    t = [i[0] for i in d]
    v = [i[1] for i in d]
    """
    ['2020', '2019', '2021', '2018', '2017']
    [47, 44, 43, 42, 14]
    """
